HTTP_LOG_DIR=~/tmp/log/http
mkdir -p $HTTP_LOG_DIR

#starts http server in cwd
http_start(){
  python -m SimpleHTTPServer > $HTTP_LOG_DIR/log.txt 2>&1 &
  echo $! > $HTTP_LOG_DIR/pid
  echo "server starts. PID=$(<$HTTP_LOG_DIR/pid)"
}
http_stop(){
  kill -9 $(<$HTTP_LOG_DIR/pid)
}
http_log(){
  tail -10 $HTTP_LOG_DIR/log.txt
}

#reloads .bashrc
brc_reload(){
  source ~/.bashrc
}

as_cd() {
  cd /storage/emulated/0/dev/automagic-assistant
}

to_lin_path() {
  WIN_PATH=$1
  LIN_PATH=$(echo "/mnt/$WIN_PATH" | sed 's/\\/\//g' | sed 's/://')
  echo $LIN_PATH
}
win_vim() {
  LIN_PATH=$(to_lin_path $1)
  vim $LIN_PATH
}
