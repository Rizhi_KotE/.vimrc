#!/bin/sh

set -e

SCRIPT_DIR=${0%%install.sh}

# put user specific vim config to default place
if  [ -f ~/.vimrc ]
then
  echo ".vimrc exists. won't install vim"
else
  if [ -d ~/.vim ]
  then
    echo ".vim exists. won't install vim"
  else
    ln -s $(realpath $SCRIPT_DIR/vim/.vimrc) ~/.vimrc
    ln -s $(realpath $SCRIPT_DIR/vim/) ~/.vim
  fi
fi

# put user specific git config to default place
if [ -f ~/.gitconfig ]
then
  echo ".gitconfig exists. won't install"
else
  ln -s $(realpath $SCRIPT_DIR/git/.gitconfig) ~/.gitconfig
fi

# put additional .bashrc to default one
if [ -d ~/.more_bash ]
then
  echo ".more_bash exists. won't install"  
else
  ln -s $(realpath $SCRIPT_DIR/more_bash) ~/.more_bash
  if grep -qxF "source ~/.more_bash/bashrc.sh" ~/.bashrc
  then
    echo ".more_bash exists in .bashrc"
  else
    echo  "source ~/.more_bash/bashrc.sh" >> ~/.bashrc
  fi
fi

# WSL make disk mount case insensitive
if [ -d /mnt/c ]
then
  if  [ ! -d /mnt/C ]
  then
   sudo ln -s /mnt/c /mnt/C
  fi
fi
